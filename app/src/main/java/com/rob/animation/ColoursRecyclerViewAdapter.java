package com.rob.animation;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ColoursRecyclerViewAdapter extends RecyclerView.Adapter<ColoursRecyclerViewAdapter.ViewHolder> {

    private ArrayList<List<String>> mData;
    private LayoutInflater mInflater;

    // data is passed into the constructor
    ColoursRecyclerViewAdapter(Context context, ArrayList<List<String>> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.colours_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        List<String> colour = mData.get(position);
        holder.myTextView.setText(colour.get(0));
        holder.myTextView.setBackgroundColor(Color.parseColor(colour.get(1)));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.colours_text);
        }

    }
}
