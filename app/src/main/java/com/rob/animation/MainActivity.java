package com.rob.animation;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class MainActivity extends AppCompatActivity {

    LottieAnimationView lottieAnimationView;
    ColoursRecyclerViewAdapter adapter;
    RecyclerView recyclerView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            switch (item.getItemId()) {
                case R.id.navigation_maps:
                    mapFragment.getView().setVisibility(View.VISIBLE);
                    lottieAnimationView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    return true;
                case R.id.navigation_lottie:
                    mapFragment.getView().setVisibility(View.GONE);
                    lottieAnimationView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    return true;
                case R.id.navigation_scroll:
                    mapFragment.getView().setVisibility(View.GONE);
                    lottieAnimationView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        lottieAnimationView = findViewById(R.id.animation_view);
        lottieAnimationView.setVisibility(View.GONE);

        // data to populate the RecyclerView with
        ArrayList<List<String>> colours = new ArrayList<>();
        colours.add(asList("TURQUOISE", "#1abc9c"));
        colours.add(asList("EMERALD", "#2ecc71"));
        colours.add(asList("PETER RIVER", "#3498db"));
        colours.add(asList("AMETHYST",  "#9b59b6"));
        colours.add(asList("WET ASPHALT",  "#34495e"));
        colours.add(asList("GREEN SEA",  "#16a085"));
        colours.add(asList("NEPHRITIS",  "#27ae60"));
        colours.add(asList("BELIZE HOLE",  "#2980b9"));
        colours.add(asList("WISTERIA",  "#8e44ad"));
        colours.add(asList("MIDNIGHT BLUE",  "#2c3e50"));
        colours.add(asList("SUN FLOWER",  "#f1c40f"));
        colours.add(asList("CARROT",  "#e67e22"));
        colours.add(asList("CARROT",  "#e67e22"));
        colours.add(asList("ALIZARIN",  "#e74c3c"));
        colours.add(asList("CLOUDS",  "#ecf0f1"));
        colours.add(asList("CONCRETE",  "#95a5a6"));
        colours.add(asList("ORANGE",  "#f39c12"));
        colours.add(asList("PUMPKIN",  "#d35400"));
        colours.add(asList("POMEGRANATE",  "#c0392b"));
        colours.add(asList("SILVER",  "#bdc3c7"));
        colours.add(asList("ASBESTOS",  "#7f8c8d"));
        colours.add(asList("TURQUOISE",  "#1abc9c"));
        colours.add(asList("EMERALD",  "#2ecc71"));
        colours.add(asList("PETER RIVER",  "#3498db"));
        colours.add(asList("AMETHYST",  "#9b59b6"));
        colours.add(asList("WET ASPHALT",  "#34495e"));
        colours.add(asList("GREEN SEA",  "#16a085"));
        colours.add(asList("NEPHRITIS",  "#27ae60"));
        colours.add(asList("BELIZE HOLE",  "#2980b9"));
        colours.add(asList("WISTERIA",  "#8e44ad"));
        colours.add(asList("MIDNIGHT BLUE",  "#2c3e50"));
        colours.add(asList("SUN FLOWER",  "#f1c40f"));
        colours.add(asList("CARROT",  "#e67e22"));
        colours.add(asList("ALIZARIN",  "#e74c3c"));
        colours.add(asList("CLOUDS",  "#ecf0f1"));
        colours.add(asList("CONCRETE",  "#95a5a6"));
        colours.add(asList("ORANGE",  "#f39c12"));
        colours.add(asList("PUMPKIN",  "#d35400"));
        colours.add(asList("POMEGRANATE",  "#c0392b"));
        colours.add(asList("SILVER",  "#bdc3c7"));
        colours.add(asList("ASBESTOS",  "#7f8c8d"));
        colours.add(asList("TURQUOISE",  "#1abc9c"));
        colours.add(asList("EMERALD",  "#2ecc71"));
        colours.add(asList("PETER RIVER",  "#3498db"));
        colours.add(asList("TURQUOISE", "#1abc9c"));
        colours.add(asList("EMERALD", "#2ecc71"));
        colours.add(asList("PETER RIVER", "#3498db"));
        colours.add(asList("AMETHYST",  "#9b59b6"));
        colours.add(asList("WET ASPHALT",  "#34495e"));
        colours.add(asList("GREEN SEA",  "#16a085"));
        colours.add(asList("NEPHRITIS",  "#27ae60"));
        colours.add(asList("BELIZE HOLE",  "#2980b9"));
        colours.add(asList("WISTERIA",  "#8e44ad"));
        colours.add(asList("MIDNIGHT BLUE",  "#2c3e50"));
        colours.add(asList("SUN FLOWER",  "#f1c40f"));
        colours.add(asList("CARROT",  "#e67e22"));
        colours.add(asList("CARROT",  "#e67e22"));
        colours.add(asList("ALIZARIN",  "#e74c3c"));
        colours.add(asList("CLOUDS",  "#ecf0f1"));
        colours.add(asList("CONCRETE",  "#95a5a6"));
        colours.add(asList("ORANGE",  "#f39c12"));
        colours.add(asList("PUMPKIN",  "#d35400"));
        colours.add(asList("POMEGRANATE",  "#c0392b"));
        colours.add(asList("SILVER",  "#bdc3c7"));
        colours.add(asList("ASBESTOS",  "#7f8c8d"));
        colours.add(asList("TURQUOISE",  "#1abc9c"));
        colours.add(asList("EMERALD",  "#2ecc71"));
        colours.add(asList("PETER RIVER",  "#3498db"));
        colours.add(asList("AMETHYST",  "#9b59b6"));
        colours.add(asList("WET ASPHALT",  "#34495e"));
        colours.add(asList("GREEN SEA",  "#16a085"));
        colours.add(asList("NEPHRITIS",  "#27ae60"));
        colours.add(asList("BELIZE HOLE",  "#2980b9"));
        colours.add(asList("WISTERIA",  "#8e44ad"));
        colours.add(asList("MIDNIGHT BLUE",  "#2c3e50"));
        colours.add(asList("SUN FLOWER",  "#f1c40f"));
        colours.add(asList("CARROT",  "#e67e22"));
        colours.add(asList("ALIZARIN",  "#e74c3c"));
        colours.add(asList("CLOUDS",  "#ecf0f1"));
        colours.add(asList("CONCRETE",  "#95a5a6"));
        colours.add(asList("ORANGE",  "#f39c12"));
        colours.add(asList("PUMPKIN",  "#d35400"));
        colours.add(asList("POMEGRANATE",  "#c0392b"));
        colours.add(asList("SILVER",  "#bdc3c7"));
        colours.add(asList("ASBESTOS",  "#7f8c8d"));
        colours.add(asList("TURQUOISE",  "#1abc9c"));
        colours.add(asList("EMERALD",  "#2ecc71"));
        colours.add(asList("PETER RIVER",  "#3498db"));

        recyclerView = findViewById(R.id.colours);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        adapter = new ColoursRecyclerViewAdapter(this, colours);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.GONE);
    }

}
